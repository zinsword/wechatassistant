# WechatAssistant

 ## Instruction

​	A Chat Bot based on chinese SNS tool Wechat. 

​	It include a **multi-wheel dialogue system**(implemented by rasa 1.10.19) , a **gossip system**(implemented by GPT)

---



## Dir

Model: chinese GPT model

Rasa: Rasa config file

Hook: Wechat hook program

MessageGate: The message dispatch implemented by Golang



