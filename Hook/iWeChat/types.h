#pragma once

#include <tchar.h>

#define MAX_IP 20
#define MAX_PORT 20

#define MAX_PATH 255
#define WECHAT_PROCESS_NAME L"WeChat.exe"
#define DLLNAME L"core.dll"

extern BOOL isAppActive;
extern BOOL isNlpActive;

typedef struct
{
	TCHAR APP_IP[MAX_IP];
	TCHAR APP_PORT[MAX_PORT];
	TCHAR NLP_IP[MAX_IP];
	TCHAR NLP_PORT[MAX_IP];
}CONFIG;


struct Information
{
	wchar_t wxid[40];		//微信ID
	wchar_t nickname[40];	//微信昵称
	/*
	wchar_t wxcount[40];	//微信账号
	wchar_t wxsex[4];		//性别
	wchar_t phone[30];		//手机号
	wchar_t device[15];		//登陆设备
	wchar_t nation[10];		//国籍
	wchar_t province[20];	//省份
	wchar_t city[20];		//城市
	wchar_t header[0x100];	//头像
	*/
};

extern struct Information INFO;

//接收的文本消息结构体
struct MessageStruct
{
	wchar_t wxid[40];
	wchar_t content[MAX_PATH];
};

//消息结构体
struct Message
{
	wchar_t type[10];		//消息类型
	wchar_t source[20];		//消息来源
	wchar_t wxid[40];		//微信ID/群ID
	wchar_t msgSender[40];	//消息发送者
	wchar_t content[200];	//消息内容
};

//@消息
struct AtMsg
{
	wchar_t chatroomid[50] = { 0 };
	wchar_t memberwxid[50] = { 0 };
	wchar_t membernickname[50] = { 0 };
	wchar_t msgcontent[100] = { 0 };
};


struct UnifiedMessage
{
	wchar_t type[10];		//消息类型
	wchar_t source[20];		//消息来源
	wchar_t wxid[40];		//微信ID/群ID
	wchar_t membernickname[50] = { 0 };		//发送者姓名
	wchar_t msgSender[40];	//消息发送者
	wchar_t content[200];	//消息内容
	wchar_t timestamp[40];  //时间戳
	wchar_t receiverID[40];
	wchar_t receiverName[40];
	UINT flag;
};


/*
struct UnifiedMessage
{
	wchar_t *type;		//消息类型
	wchar_t *source;		//消息来源
	wchar_t *wxid;		//微信ID/群ID
	wchar_t *membernickname;		//发送者姓名
	wchar_t *msgSender;	//消息发送者
	wchar_t *content;	//消息内容
	UINT flag;
};
*/