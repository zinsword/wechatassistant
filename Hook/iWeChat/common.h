#pragma once

#include "stdafx.h"

void getTimestamp(TCHAR *obj);

void loadConfig(CONFIG &cf);

DWORD wechatPID(LPCWSTR process_name);

char * wchar2char(const wchar_t* wchar);

wchar_t * char2wchar(const char* cchar);

std::wstring UTF8ToUnicode(const std::string & s);

std::string WChar2Ansi(LPCWSTR pwszSrc);

std::string buildJsonMsg(UnifiedMessage *msg);

UnifiedMessage * parseJsonMsg(std::string msg);

// wchar_t to string
void Wchar_tToString(std::string& szDst, wchar_t *wchar);

// string to wstring
void StringToWstring(std::wstring& szDst, std::string str);
