#pragma once

extern CRITICAL_SECTION recv_mutex, send_mutex;
extern WCHAR USERNAME[40];

void getUserInfo();

void recvOneMsgFromWechat(UnifiedMessage* msg);

void sendOneMsgToWechat(std::string msg);

UINT recvMessageFromWechat(LPVOID pParam);

UINT sendMessageToWechat(LPVOID);

void sendGroupMessageToWechat(UnifiedMessage* uMsg);

void sendAtMessageToWechat(UnifiedMessage* uMsg);

UINT testStrToJson(std::string _msg);