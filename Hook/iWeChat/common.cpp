#include "stdafx.h"
#include "common.h"
/*
#include <rapidjson\document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
*/
#include <json\json.h>
#include <json\reader.h>
#include <json\writer.h>
#include <json\value.h>

Json::Value readValue;
//readValue["init"] = "";
Json::String err;
Json::CharReaderBuilder reader;
std::unique_ptr<Json::CharReader>const json_read(reader.newCharReader());

void getTimestamp(TCHAR *obj)
{
	/*
	time_t now = time(0);
	char timestamp[35];
	ctime_s(timestamp, 35 , &now);
	TCHAR *wtimestamp = char2wchar(timestamp);
	wcscpy_s(obj, 40, wtimestamp);
	*/

	SYSTEMTIME time;
	GetLocalTime(&time);
	char dateTimeStr[200] = { 0 };
	sprintf_s(dateTimeStr, "%d/%02d/%02d_%02d:%02d:%02d", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

	wcscpy_s(obj, 40, char2wchar(dateTimeStr));
}

void loadConfig(CONFIG &cf)
{
	//获取当前工作目录下的dll
	WCHAR szPath[MAX_PATH] = { 0 };
	WCHAR* buffer = szPath;
	//if ((buffer = _getcwd(NULL, 0)) == NULL)
	if (!GetCurrentDirectory(MAX_PATH, szPath))
	{
		MessageBox(NULL, L"Get file path failed!", L"Error", 0);
		//return FALSE;
	}
	WCHAR *fileName = L"config.ini";
	swprintf_s(szPath, L"%s\\%s", buffer, fileName);

	WCHAR app_ip[MAX_IP] = { 0 };
	WCHAR app_port[MAX_PORT] = { 0 };
	WCHAR nlp_ip[MAX_IP] = { 0 };
	WCHAR nlp_port[MAX_PORT] = { 0 };
	GetPrivateProfileString(L"UPSTREAM SERVER", L"APP_SERVER", L"192.168.0.1", app_ip, MAX_IP, szPath);
	GetPrivateProfileString(L"UPSTREAM SERVER", L"APP_PORT", L"6666", app_port, MAX_PORT, szPath);
	GetPrivateProfileString(L"UPSTREAM SERVER", L"NLP_SERVER", L"192.168.0.1", nlp_ip, MAX_IP, szPath);
	GetPrivateProfileString(L"UPSTREAM SERVER", L"NLP_PORT", L"6666", nlp_port, MAX_PORT, szPath);
	
	swprintf_s(cf.APP_IP, L"%s", app_ip);
	swprintf_s(cf.APP_PORT, L"%s", app_port);
	swprintf_s(cf.NLP_IP, L"%s", nlp_ip);
	swprintf_s(cf.NLP_PORT, L"%s", nlp_port);
	//debug
	//wcscat_s(ip, port);
	//MessageBox(NULL, ip, L"Load Upstream Server Info", 0);
}

DWORD wechatPID(LPCWSTR process_name)
{
	HANDLE process_all = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 process_info = { 0 };
	process_info.dwSize = sizeof(PROCESSENTRY32);
	do
	{
		if (wcscmp(process_name, process_info.szExeFile) == 0)
		{
			return process_info.th32ProcessID;
		}
	} while (Process32Next(process_all, &process_info));

	return 0;
}


char * wchar2char(const wchar_t* wchar)

{
	char * m_char;

	int len = WideCharToMultiByte(CP_ACP, 0, wchar, wcslen(wchar), NULL, 0, NULL, NULL);

	m_char = new char[len + 1];

	WideCharToMultiByte(CP_ACP, 0, wchar, wcslen(wchar), m_char, len, NULL, NULL);

	m_char[len] = '\0';

	return m_char;

}

wchar_t * char2wchar(const char* cchar)

{
	wchar_t *m_wchar;

	int len = MultiByteToWideChar(CP_ACP, 0, cchar, strlen(cchar), NULL, 0);

	m_wchar = new wchar_t[len + 1];

	MultiByteToWideChar(CP_ACP, 0, cchar, strlen(cchar), m_wchar, len);

	m_wchar[len] = '\0';

	return m_wchar;

}

std::string buildJsonMsg(UnifiedMessage *msg)
{
	Json::Value jsonMsg;
	Json::StreamWriterBuilder builder;
	builder.settings_["emitUTF8"] = true;
	builder.settings_["indentation"] = "";
	std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
	std::ostringstream os;

	/*
	if (StrStrI(msg->content, L"@AI助理小周"))
	{
		msg->flag = 1;
		wchar_t *buffer;
		wchar_t* newValue = wcstok_s(msg->content, L"@AI助理小周", &buffer);
		wcscpy_s(msg->content, 200, newValue + 1);
	}
	else
	{
		msg->flag = 0;
	}
	*/
	//TODO: bug-fix Message clean 
	//sapce, quote, etc
	wchar_t *buffer;
	wchar_t* newValue = wcstok_s(msg->content, L" ", &buffer);
	wchar_t newContent = (const wchar_t)L"";
	//ZeroMemory(msg->content, sizeof(msg->content));
	wchar_t temp[200] = { 0 };
	while (newValue)
	{
		//wcscat_s(&newContent, sizeof(newValue), newValue);
		swprintf_s(temp, L"%s%s", temp, newValue);
		newValue = wcstok_s(NULL, L" ", &buffer);
	}
	wcscpy_s(msg->content, 200, temp);
	
	jsonMsg["source"] = wchar2char(msg->source);
	jsonMsg["wxid"] = wchar2char(msg->wxid);
	jsonMsg["msgSender"] = wchar2char(msg->msgSender);
	jsonMsg["membernickname"] = wchar2char(msg->membernickname);
	jsonMsg["content"] = wchar2char(msg->content);
	jsonMsg["type"] = wchar2char(msg->type);
	//Add time stamp
	jsonMsg["timestamp"] = wchar2char(msg->timestamp);
	//Add receiver id\name
	jsonMsg["receiverID"] = wchar2char(msg->receiverID);
	jsonMsg["receiverName"] = wchar2char(msg->receiverName);


	writer->write(jsonMsg, &os);
	std::string json_file = os.str();
	// Replace all space to '_'
	//json_file = json_file.replace(json_file.find(" "), 1, "_");
	return json_file;
}

UnifiedMessage* parseJsonMsg(std::string msg)
{
	std::string newMsg = msg;// msg.assign(1, msg.length() - 1);
	std::unique_ptr<Json::CharReader>const json_read(reader.newCharReader());
	bool result = json_read->parse(newMsg.c_str(), newMsg.c_str() + newMsg.length(), &readValue, &err);
	if (!result || !err.empty())
	{
		MessageBoxA(NULL, err.c_str(), "JSON", 0);
		//return NULL;
		exit(-1);
	}

	UnifiedMessage *uMsg = new UnifiedMessage;
	
	TCHAR* source = char2wchar(readValue["source"].asCString());
	TCHAR* type = char2wchar(readValue["type"].asCString());
	TCHAR* wxid = char2wchar(readValue["wxid"].asCString());
	TCHAR* membernickname = char2wchar(readValue["membernickname"].asCString());
	TCHAR* msgSender = char2wchar(readValue["msgSender"].asCString());
	TCHAR* content = char2wchar(readValue["content"].asCString());
	// Add timestamp receiver id/name
	TCHAR* timestamp = char2wchar(readValue["timestamp"].asCString());
	TCHAR* receiverID = char2wchar(readValue["receiverID"].asCString());
	TCHAR* receiverName = char2wchar(readValue["receiverName"].asCString());
	
	
	wcscpy_s(uMsg->source, 20, source);
	wcscpy_s(uMsg->type, 10, type);
	wcscpy_s(uMsg->wxid, 40, wxid);
	wcscpy_s(uMsg->membernickname, 50, membernickname);
	wcscpy_s(uMsg->content, 200, content);
	wcscpy_s(uMsg->msgSender, 40, msgSender);
	// TODO
	// Add timestamp receiver id/name below if needed
	uMsg->flag = 0;


	return uMsg;
}


std::wstring UTF8ToUnicode(const std::string & s)
{
	std::wstring result;

	int n = MultiByteToWideChar(CP_UTF8, 0, s.c_str(), -1, NULL, 0);
	wchar_t * buffer = new wchar_t[n];

	::MultiByteToWideChar(CP_UTF8, 0, s.c_str(), -1, buffer, n);

	result = buffer;
	delete[] buffer;

	return result;
}


std::string WChar2Ansi(LPCWSTR pwszSrc)
{
	int nLen = 0;
	nLen = WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, NULL, 0, NULL, NULL);
	if (nLen <= 0)
		return std::string("");

	char* pszDst = new char[nLen];
	if (NULL == pszDst)
		return std::string("");

	WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, pszDst, nLen, NULL, NULL);
	pszDst[nLen - 1] = 0;
	std::string strTemp(pszDst);
	delete[] pszDst;

	return strTemp;
}

// wchar_t to string
void Wchar_tToString(std::string& szDst, wchar_t *wchar)
{
	wchar_t * wText = wchar;
	DWORD dwNum = WideCharToMultiByte(CP_OEMCP, NULL, wText, -1, NULL, 0, NULL, FALSE);// WideCharToMultiByte的运用
	char *psText;  // psText为char*的临时数组，作为赋值给std::string的中间变量
	psText = new char[dwNum];
	WideCharToMultiByte(CP_OEMCP, NULL, wText, -1, psText, dwNum, NULL, FALSE);// WideCharToMultiByte的再次运用
	szDst = psText;// std::string赋值
	delete[]psText;// psText的清除
}

// string to wstring
void StringToWstring(std::wstring& szDst, std::string str)
{
	std::string temp = str;
	int len = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)temp.c_str(), -1, NULL, 0);
	wchar_t * wszUtf8 = new wchar_t[len + 1];
	memset(wszUtf8, 0, len * 2 + 2);
	MultiByteToWideChar(CP_ACP, 0, (LPCSTR)temp.c_str(), -1, (LPWSTR)wszUtf8, len);
	szDst = wszUtf8;
	std::wstring r = wszUtf8;
	delete[] wszUtf8;
}
