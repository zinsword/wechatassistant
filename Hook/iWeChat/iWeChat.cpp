// iWeChat.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "iWeChat.h"
#include "common.h"
#include "inject.h"
#include "wechat.h"
#include "redis.h"

#pragma comment(lib, "hiredis.lib")
#pragma comment(lib, "lua.lib")
#pragma comment(lib, "Win32_Interop.lib")

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
CONFIG myConfig;

BOOL isAppActive = false;
BOOL isNlpActive = false;

HANDLE hRecvThread;
HANDLE hSendThread;
HANDLE hSubRedisThread;

struct Information INFO = {};


INT_PTR CALLBACK mainProc(HWND Arg1, UINT Arg2, WPARAM Arg3, LPARAM Arg4
)
{
	try {
		if (Arg2 == WM_INITDIALOG)
		{
			loadConfig(myConfig);
			//MessageBox(NULL, myConfig.IP, myConfig.PORT, 0);
			SetDlgItemText(Arg1, IDC_APP_IPADDRESS, (myConfig.APP_IP));
			SetDlgItemText(Arg1, IDC_APP_PORT, (myConfig.APP_PORT));
			SetDlgItemText(Arg1, IDC_NLP_IPADDRESS, (myConfig.NLP_IP));
			SetDlgItemText(Arg1, IDC_NLP_PORT, (myConfig.NLP_PORT));
			setNlpServer(wchar2char(myConfig.APP_IP), wchar2char(myConfig.APP_PORT));
			setNlpServer(wchar2char(myConfig.NLP_IP), wchar2char(myConfig.NLP_PORT));

			hRecvThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)recvMessageFromWechat, NULL, 0, NULL);
			hSubRedisThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)recvMsgFromRedis, NULL, 0, NULL);
			hSendThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)sendMessageToWechat, NULL, 0, NULL);

			//

		}

		if (Arg2 == WM_CLOSE)
		{
			//TODO: bug fix
			/*
			//获取微信Pid
			DWORD dwPid = wechatPID(WECHAT_PROCESS_NAME);
			if (dwPid != 0)
			{
				if (!CheckIsInject(dwPid))
				{
					uninjectDll();
				}
			}
			*/
			//WaitForSingleObject(hRecvThread, INFINITE);
			EndDialog(Arg1, NULL);
		}

		if (Arg2 == WM_COMMAND)
		{

			if (Arg3 == IDC_APP_CHECK)
			{
				UINT curState = IsDlgButtonChecked(Arg1, IDC_APP_CHECK);
				if (curState == BST_UNCHECKED)
				{
					//CheckDlgButton(Arg1, IDC_APP_CHECK, BST_CHECKED);
					isAppActive = true;
				}

				if (curState == BST_CHECKED)
				{
					//CheckDlgButton(Arg1, IDC_APP_CHECK, BST_UNCHECKED);
					isAppActive = false;
				}

				//isAppActive = IsDlgButtonChecked(Arg1, IDC_APP_CHECK);
			}

			if (Arg3 == IDC_NLP_CHECK)
			{
				if (IsDlgButtonChecked(Arg1, IDC_NLP_CHECK))
				{
					isNlpActive = true;
				}
				else
				{
					isNlpActive = false;
				}
			}

		}

		if (Arg2 == WM_COPYDATA)
		{
			//MessageBox(NULL, L"Test", L"Test", 0);
			COPYDATASTRUCT *pCopyData = (COPYDATASTRUCT*)Arg4;
			switch (pCopyData->dwData)
			{
				//获取个人信息
			case WM_GetInformation:
			{
				//接收消息
				Information *info = new Information;
				info = (Information*)pCopyData->lpData;

				//显示到控件
				TCHAR* wxid = info->wxid;
				TCHAR* nickname = info->nickname;

				wcscpy_s(INFO.wxid, info->wxid);
				wcscpy_s(INFO.nickname, info->nickname);

				SetDlgItemText(Arg1, IDC_NAME, nickname);
				SetDlgItemText(Arg1, IDC_ID, wxid);

			}
			break;

			case WM_ShowChatRecord:
			{
				Message *msg = new Message;
				msg = (Message*)pCopyData->lpData;

				UnifiedMessage * oneMsg = new UnifiedMessage;
				wcscpy_s(oneMsg->source, 20, msg->source);
				wcscpy_s(oneMsg->type, 10, msg->type);
				wcscpy_s(oneMsg->content, 200, msg->content);
				wcscpy_s(oneMsg->msgSender, 40, msg->msgSender);
				wcscpy_s(oneMsg->wxid, 40, msg->wxid);
				wcscpy_s(oneMsg->membernickname, 50, L"");
				wcscpy_s(oneMsg->receiverID, 40, INFO.wxid);
				wcscpy_s(oneMsg->receiverName, 40, INFO.nickname);
				getTimestamp(oneMsg->timestamp);
				oneMsg->flag = NONE;

				/*
				if (isAppActive)
				{
					oneMsg->flag = oneMsg->flag | APP;
				}
				if (isNlpActive)
				{
					oneMsg->flag = oneMsg->flag | NLP;
				}
				*/
				recvOneMsgFromWechat(oneMsg);
			}
			break;

			case WM_NotLogin:
			{
				MessageBox(NULL, L"Not login!\nPlease Login & restart", L"Tip", 0);
				exit(-1);
			}
			break;

			default:

				break;
			}
		}
	
		//testStrToJson("");
		return  FALSE;
	}
	catch (std::exception &e) {
		MessageBoxA(NULL, e.what(), "Exception", 0);
	}
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	//获取微信Pid
	DWORD dwPid = wechatPID(WECHAT_PROCESS_NAME);
	if (dwPid == 0)
	{
		MessageBox(NULL, L"Can not find WeChat.exe!\nPlease run it first!", L"Error", 0);
		return FALSE;
	}

	if (!CheckIsInject(dwPid))
	{
		uninjectDll();
	}

	injectDll();
	//Sleep(500);
	//test();
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, mainProc);
	
	return 0;
}


