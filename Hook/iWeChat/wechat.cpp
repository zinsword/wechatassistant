#include "stdafx.h"
#include "wechat.h"
#include "redis.h"
#include "common.h"
#include "types.h"

//#include <rapidjson\rapidjson.h>
#include <json\json.h>

CRITICAL_SECTION recv_mutex, send_mutex;

std::queue<UnifiedMessage*> RECVFROMWECHAT;
//std::queue<UnifiedMessage*> SENDTOWECHAT;
std::queue<std::string> SENDTOWECHAT;

WCHAR USERNAME[40];

void setUserName(WCHAR* _userName)
{
	wcscpy_s(USERNAME, 40, _userName);
}

WCHAR* getUserName()
{
	return USERNAME;
}



void recvOneMsgFromWechat(UnifiedMessage* msg)
{
	InitializeCriticalSection(&recv_mutex);
	RECVFROMWECHAT.push(msg);
	DeleteCriticalSection(&recv_mutex);
}

void sendOneMsgToWechat(std::string msg)
{
	InitializeCriticalSection(&send_mutex);
	SENDTOWECHAT.push(msg);
	DeleteCriticalSection(&send_mutex);
}

void getUserInfo()
{
	HWND hWnd = FindWindow(NULL, L"iWeChatCore");
	if (hWnd == NULL)
	{
		MessageBoxA(NULL, "Can not find iWeChat core module!", "Error", MB_OK);
		return;
	}
	COPYDATASTRUCT GetInformation;
	//组装数据
	GetInformation.dwData = WM_GetInformation;
	GetInformation.cbData = 0;
	GetInformation.lpData = NULL;
	//发送获取个人信息消息
	SendMessage(hWnd, WM_COPYDATA, NULL, (LPARAM)&GetInformation);
}

UINT recvMessageFromWechat(LPVOID pParam)
{
	while (true)
	{
		if (RECVFROMWECHAT.empty())
		{
			Sleep(1000);
			continue;
		}

		InitializeCriticalSection(&recv_mutex);
		UnifiedMessage* msg = (UnifiedMessage*)RECVFROMWECHAT.front();

		std::string jsonMsg = buildJsonMsg(msg);
		//MessageBoxA(NULL, jsonMsg.c_str(), "", 0);

		if (isAppActive)
		{
			sendMsgToRedis(getAppIp(), getAppPort(), APP_RECV_FROM_WECHAT, jsonMsg);
		}
		if (isNlpActive)
		{
			sendMsgToRedis(getNlpIp(), getNlpPort(), NLP_RECV_FROM_WECHAT, jsonMsg);
		}

		delete msg;
		msg = NULL;
		RECVFROMWECHAT.pop();
		DeleteCriticalSection(&recv_mutex);
	}
}


UINT sendMessageToWechat(LPVOID)
{
	while(true){
		if (SENDTOWECHAT.empty())
		{
			Sleep(1000);
			continue;
		}

		InitializeCriticalSection(&send_mutex);
		std::string msg = SENDTOWECHAT.front();

		UnifiedMessage* uMsg = parseJsonMsg(msg);
		if (uMsg == NULL)
		{
			return 2;
			//continue;
		}
		//MessageBox(NULL, uMsg->content, L"", 0);
		sendGroupMessageToWechat(uMsg);
		/*
		if (wcscmp(uMsg->membernickname, L"") != 0)
		{
			sendGroupMessageToWechat(uMsg);
		}
		else
		{
			sendAtMessageToWechat(uMsg);
		}
		*/

		delete uMsg;		
		uMsg = NULL;
		
		SENDTOWECHAT.pop();
		DeleteCriticalSection(&send_mutex);
	}
	return 0;
}

UINT testStrToJson(std::string _msg)
{
	/*
		if (SENDTOWECHAT.empty())
		{
			//Sleep(1000);
			return 1;
		}

		InitializeCriticalSection(&send_mutex);
		std::string msg = SENDTOWECHAT.front();
	
		
		
		bool result = json_read->parse(msg.c_str(), msg.c_str() + msg.length(), &readValue, &err);
		if (!result || !err.empty())
		{
			MessageBoxA(NULL, err.c_str(), "JSON", 0);
			return NULL;
		}

		const char * test = readValue["content"].asCString();
		//MessageBoxA(NULL, test , "Tip", 0);
		

		SENDTOWECHAT.pop();
		DeleteCriticalSection(&send_mutex);
		*/
	return 0;
	
}

void sendGroupMessageToWechat(UnifiedMessage* uMsg)
{
	//填充数据到结构体
	MessageStruct *message = new MessageStruct;

	//wcscpy_s(message->wxid, wcslen(uMsg->wxid) + 1, uMsg->wxid);
	//wcscpy_s(message->content, wcslen(uMsg->content) + 1, uMsg->content);
	wcscpy_s(message->wxid, 40, uMsg->wxid);
	wcscpy_s(message->content, 255, uMsg->content);
	//wcscpy_s(message->content, 5, L"你好中国");

	//查找窗口
	HWND pWnd = FindWindow(NULL, L"iWeChatCore");

	COPYDATASTRUCT MessageData;
	MessageData.dwData = WM_SendTextMessage;
	MessageData.cbData = sizeof(MessageStruct);
	MessageData.lpData = message;

	//发送消息
	SendMessage(pWnd, WM_COPYDATA, NULL, (LPARAM)&MessageData);
}

void sendAtMessageToWechat(UnifiedMessage* uMsg)
{

	AtMsg* msg = new AtMsg;
	wcscpy_s(msg->chatroomid, wcslen(uMsg->wxid) + 1, uMsg->wxid);
	wcscpy_s(msg->memberwxid, wcslen(uMsg->msgSender) + 1, uMsg->msgSender);
	wcscpy_s(msg->membernickname, wcslen(uMsg->membernickname) + 1, uMsg->membernickname);
	wcscpy_s(msg->msgcontent, wcslen(uMsg->content) + 1, uMsg->content);


	HWND pWnd = FindWindow(NULL, L"iWeChatCore");
	COPYDATASTRUCT atmsgdata;
	atmsgdata.dwData = WM_SendAtMsg;
	atmsgdata.cbData = sizeof(AtMsg);
	atmsgdata.lpData = msg;
	//发送消息
	SendMessage(pWnd, WM_COPYDATA, NULL, (LPARAM)&atmsgdata);

}
