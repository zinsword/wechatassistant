#pragma once

#include "hiredis.h"

extern const char* Auth_CMD;

void test(char*);

void setAppServer(CHAR* ip, CHAR* port);

CHAR *getAppIp();

CHAR *getAppPort();

void setNlpServer(CHAR* ip, CHAR* port);

CHAR *getNlpIp();

CHAR *getNlpPort();

void sendMsgToRedis(const CHAR* ip, const CHAR* port, const CHAR* queue, std::string msg);

UINT recvMsgFromRedis(LPVOID pParam);

