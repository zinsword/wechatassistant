#include "stdafx.h"
#include "redis.h"
#include "common.h"
#include "wechat.h"

CHAR APP_IP[MAX_IP];
CHAR APP_PORT[MAX_PORT];
CHAR NLP_IP[MAX_IP];
CHAR NLP_PORT[MAX_PORT];

const char* Auth_CMD = "Auth AINLP_6066";

const char *APP_RECV_FROM_WECHAT = "APP_RECV_FROM_WECHAT";
const char *APP_SEND_TO_WECHAT = "APP_SEND_TO_WECHAT";

const char *NLP_RECV_FROM_WECHAT = "NLP_RECV_FROM_WECHAT";
const char *NLP_SEND_TO_WECHAT = "NLP_SEND_TO_WECHAT";

void setAppServer(CHAR* ip, CHAR* port)
{
	memcpy(APP_IP, ip, MAX_IP);
	memcpy(APP_PORT, port, MAX_PORT);
}

CHAR *getAppIp()
{
	return APP_IP;
}

CHAR *getAppPort()
{
	return APP_PORT;
}

void setNlpServer(CHAR* ip, CHAR* port)
{
	memcpy(NLP_IP, ip, MAX_IP);
	memcpy(NLP_PORT, port, MAX_PORT);
}

CHAR *getNlpIp()
{
	return NLP_IP;
}

CHAR *getNlpPort()
{
	return NLP_PORT;
}

void sendMsgToRedis(const CHAR* ip, const CHAR* port, const CHAR* queue, std::string msg)
{
	if ((strcmp(queue, NLP_RECV_FROM_WECHAT) != 0) && (strcmp(queue, APP_RECV_FROM_WECHAT) != 0))
	{
		MessageBoxA(NULL, queue, "Wrong queue name!", 0);
		return;
	}

	redisContext* c = redisConnect(getNlpIp(), atoi(getNlpPort()));
	if (c->err)
	{
		MessageBox(NULL, char2wchar(c->errstr), L"Connect to redisServer faile", 0);
		redisFree(c);
		return;
	}

	char* command = (char*)Auth_CMD;
	redisReply* r = (redisReply*)redisCommand(c, command);

	if (NULL == r)
	{
		MessageBox(NULL, L"Execut command1 failure", L"Error", 0);
		redisFree(c);
		return;
	}
	if (!(r->type == REDIS_REPLY_STATUS && (strcmp(r->str, "OK") == 0 || strcmp(r->str, "ok") == 0)))
	{
		MessageBox(NULL, L"Failed to execute command", char2wchar(command), 0);
		freeReplyObject(r);
		redisFree(c);
		return;
	}

	std::string command2("PUBLISH ");
	command2.append(queue);
	command2.append(" \'");
	command2.append(msg);
	command2.append("\'");
	r = (redisReply*)redisCommand(c, command2.c_str());

	/*
	** {"content":"测试时间戳","membernickname":"","msgSender":"NULL","source":"好友消息","timestamp":"Tue Jan  5 16:15:36 2021\n","type":"文字","wxid":"wxid_a8lm5k13l30k52"}
	** TODO:bug-fix
	** if a string has space in it , such as " ", "\t', the string will be split ,cause the publish command failed.
	*/

	if (NULL == r)
	{
		MessageBox(NULL, L"Execut command1 failure", L"Error", 0);
		redisFree(c);
		return;
	}
	// TODO:bug-fix
	// r-integer value
	//if (!((r->type == REDIS_REPLY_INTEGER) && (r->integer == 2)))
	if (!((r->type == REDIS_REPLY_INTEGER)))
	{
		MessageBox(NULL, char2wchar(r->str), L"Failed to execute command", 0);
		freeReplyObject(r);
		redisFree(c);
		return;
	}
	freeReplyObject(r);
}

UINT recvMsgFromRedis(LPVOID pParam)
{
	redisContext* rc = redisConnect(getNlpIp(), atoi(getNlpPort()));
	if (rc == NULL || rc->err)
	{
		return 1;
	}

	redisReply *reply = (redisReply*)redisCommand(rc, Auth_CMD);
	freeReplyObject(reply);

	reply = (redisReply*)redisCommand(rc, "SUBSCRIBE NLP_SEND_TO_WECHAT");
	freeReplyObject(reply);
	while (redisGetReply(rc, (void **)&reply) == REDIS_OK)
	{
		if (reply == NULL)
		{
			MessageBox(NULL, L"return 2;", L"return 2;", 0);
			return 2;
		}

		if (reply->type == REDIS_REPLY_ERROR)
		{
			MessageBox(NULL, L"Error: return [REDIS_REPLY_ERROR]", char2wchar(reply->str), 0);
		}
		else if (reply->type == REDIS_REPLY_STATUS)
		{
			if (_stricmp(reply->str, "OK") != 0)
			{
				MessageBox(NULL, L"Error: return [REDIS_REPLY_STATUS]", char2wchar(reply->str), 0);
				freeReplyObject(reply);
				return 3;
			}
			printf("return [REDIS_REPLY_STATUS]: %d\n", reply->str);
		}
		else if (reply->type == REDIS_REPLY_INTEGER)
		{
			printf("return [REDIS_REPLY_INTEGER]: %l64d\n", reply->integer);
		}
		else if (reply->type == REDIS_REPLY_STRING)
		{
			printf("return [REDIS_REPLY_STRING]: %s\n", reply->str);
		}
		else if (reply->type == REDIS_REPLY_ARRAY)
		{
			//for (int i = 0; i < reply->elements; i++)
			//{
			//printf("%d)%s\n", i + 1, reply->element[i]->str);
			//MessageBox(NULL, char2wchar(reply->element[i]->str), L"Error: return [REDIS_REPLY_STATUS]",  0);
			//}
			std::string msg = reply->element[2]->str;
			//UnifiedMessage* uMsg = parseJsonMsg(msg);
			sendOneMsgToWechat(msg);
		}
		else if (reply->type == REDIS_REPLY_NIL)
		{
			printf("return [REDIS_REPLY_NIL]: %s\n", reply->str);
		}
		freeReplyObject(reply);
	}
	redisFree(rc);
}

void test(char*)
{
	//redis默认监听端口为6387 可以再配置文件中修改
	redisContext* c = redisConnect(getNlpIp(), atoi(getNlpPort()));
	if (c->err)
	{
		MessageBox(NULL, char2wchar(c->errstr), L"Connect to redisServer faile",0);
		redisFree(c);
		return;
	}

	char* command = (char*)Auth_CMD;
	redisReply* r = (redisReply*)redisCommand(c, command);

	if (NULL == r)
	{
		MessageBox(NULL, L"Execut command1 failure", L"Error", 0);
		redisFree(c);
		return;
	}
	if (!(r->type == REDIS_REPLY_STATUS && (strcmp(r->str, "OK") == 0 || strcmp(r->str, "ok") == 0)))
	{
		MessageBox(NULL, L"Failed to execute command", char2wchar(command), 0);
		freeReplyObject(r);
		redisFree(c);
		return;
	}

	command = "PUBLISH NLP_RECV_FROM_WECHAT Test_Message";
	r = (redisReply*)redisCommand(c, command);

	if (NULL == r)
	{
		MessageBox(NULL, L"Execut command1 failure", L"Error", 0);
		redisFree(c);
		return;
	}
	if (!((r->type == REDIS_REPLY_INTEGER) && (r->integer == 1)))
	{
		MessageBox(NULL, L"Failed to execute command", L"Error", 0);
		freeReplyObject(r);
		redisFree(c);
		return;
	}

	freeReplyObject(r);
}