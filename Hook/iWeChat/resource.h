//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by iWeChat.rc
//
#define IDC_MYICON                      2
#define IDD_IWECHAT_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDI_IWECHAT                     107
#define IDI_SMALL                       108
#define IDC_IWECHAT                     109
#define IDR_MAINFRAME                   128
#define IDD_MAIN                        129
#define IDC_IPADDRESS                   1000
#define IDC_SEND_IPADDRESS              1000
#define IDC_APP_IPADDRESS               1000
#define IDC_RECV_IPADDRESS              1001
#define IDC_NLP_IPADDRESS               1001
#define IDC_ID                          1002
#define IDC_NAME                        1003
#define IDC_SEND_PORT                   1004
#define IDC_APP_PORT                    1004
#define IDC_RECV_PORT                   1005
#define IDC_NLP_PORT                    1005
#define IDC_APP_CHECK                   1007
#define IDC_CHECK3                      1008
#define IDC_NLP_CHECK                   1008
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
