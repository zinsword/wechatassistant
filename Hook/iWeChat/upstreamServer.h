#pragma once

#define NONE 0
#define APP 1
#define NLP 2

extern const char *APP_RECV_FROM_WECHAT;
extern const char *APP_SEND_TO_WECHAT;

extern const char *NLP_RECV_FROM_WECHAT;
extern const char *NLP_SEND_TO_WECHAT;