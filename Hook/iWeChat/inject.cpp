#include "stdafx.h"
#include "inject.h"
#include "common.h"

//#define DLLNAME "core.dll"

BOOL CheckIsInject(DWORD dwProcessid)
{
	HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
	//初始化模块信息结构体
	MODULEENTRY32 me32 = { sizeof(MODULEENTRY32) };
	//创建模块快照 1 快照类型 2 进程ID
	hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessid);
	//如果句柄无效就返回false
	if (hModuleSnap == INVALID_HANDLE_VALUE)
	{
		MessageBox(NULL, L"Create snapshot failed!", L"Error", MB_OK);
		return FALSE;
	}
	//通过模块快照句柄获取第一个模块的信息
	if (!Module32First(hModuleSnap, &me32))
	{
		MessageBox(NULL, L"Get first module32 failed!", L"Error", MB_OK);
		//获取失败则关闭句柄
		CloseHandle(hModuleSnap);
		return FALSE;
	}
	do
	{
		//std::cout << me32.szModule;
		//MessageBox(NULL, me32.szModule, "Note", MB_OK);
		if (StrCmp(me32.szModule, DLLNAME) == 0)
		{
			return FALSE;
		}

	} while (Module32Next(hModuleSnap, &me32));
	return TRUE;
}

BOOL injectDll()
{
	//获取当前工作目录下的dll
	TCHAR szPath[MAX_PATH] = { 0 };
	TCHAR* buffer = szPath;
	//if ((buffer = _getcwd(NULL, 0)) == NULL)
	if (!GetCurrentDirectory(MAX_PATH, szPath))
	{
		MessageBox(NULL, L"Get file path failed!", L"Error", 0);
		return FALSE;
	}
	swprintf_s(szPath, L"%s\\%s", buffer, DLLNAME);
	//MessageBoxA(NULL, szPath, "Note", 0);

	//获取微信Pid
	DWORD dwPid = wechatPID(WECHAT_PROCESS_NAME);
	if (dwPid == 0)
	{
		MessageBoxA(NULL, "Can not find WeChat.exe! Please run it first!", "Error", 0);
		return FALSE;
	}
	//检测dll是否已经注入
	if (CheckIsInject(dwPid))
	{
		//打开进程
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPid);
		if (hProcess == NULL)
		{
			MessageBoxA(NULL, "Open WeChat Process Failed", "Error", 0);
			return FALSE;
		}
		//在微信进程中申请内存
		LPVOID pAddress = VirtualAllocEx(hProcess, NULL, MAX_PATH, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		if (pAddress == NULL)
		{
			MessageBoxA(NULL, "Alloc memory failed!", "Error", 0);
			return FALSE;
		}
		//写入dll路径到微信进程
		DWORD dwWrite = 0;
		if (WriteProcessMemory(hProcess, pAddress, wchar2char(szPath), MAX_PATH, &dwWrite) == 0)
		{
			MessageBoxA(NULL, "Write path failed!", "Error", 0);
			return FALSE;
		}

		//获取LoadLibraryA函数地址
		FARPROC pLoadLibraryAddress = GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
		if (pLoadLibraryAddress == NULL)
		{
			MessageBoxA(NULL, "Fetch LoadLibraryA address failed!", "Error", 0);
			return FALSE;
		}
		//远程线程注入dll
		HANDLE hRemoteThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)pLoadLibraryAddress, pAddress, 0, NULL);
		if (hRemoteThread == NULL)
		{
			MessageBoxA(NULL, "Create remote thread failed!", "Error", 0);
			return FALSE;
		}

		CloseHandle(hRemoteThread);
		CloseHandle(hProcess);
	}
	else
	{
		MessageBoxA(NULL, "DLL has been injected, don't repeat it!", "Note", 0);
		return FALSE;
	}

	return TRUE;
}


void uninjectDll()
{
	//获取微信Pid
	DWORD dwPid = wechatPID(WECHAT_PROCESS_NAME);
	if (dwPid == 0)
	{
		MessageBoxA(NULL, "Can not find WeChat process!", "Error", 0);
		return;
	}

	//遍历模块
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPid);
	MODULEENTRY32 ME32 = { 0 };
	ME32.dwSize = sizeof(MODULEENTRY32);
	BOOL isNext = Module32First(hSnap, &ME32);
	BOOL flag = FALSE;
	while (isNext)
	{
		USES_CONVERSION;
		if (wcscmp(ME32.szModule, DLLNAME) == 0)
		{
			flag = TRUE;
			break;
		}
		isNext = Module32Next(hSnap, &ME32);
	}
	if (flag == FALSE)
	{
		MessageBoxA(NULL, "Can not find object module!", "Error", 0);
		return;
	}

	//打开目标进程
	HANDLE hPro = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPid);
	//获取FreeLibrary函数地址
	FARPROC pFun = GetProcAddress(GetModuleHandleA("kernel32.dll"), "FreeLibrary");

	//创建远程线程执行FreeLibrary
	HANDLE hThread = CreateRemoteThread(hPro, NULL, 0, (LPTHREAD_START_ROUTINE)pFun, ME32.modBaseAddr, 0, NULL);
	if (!hThread)
	{
		MessageBoxA(NULL, "Create remote thread failed!", "Error", 0);
		return;
	}

	CloseHandle(hSnap);
	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);
	CloseHandle(hPro);
	//MessageBoxA(NULL, "Dll uninject succeed", "Tip", 0);
}