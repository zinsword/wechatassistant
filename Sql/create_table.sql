PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "USER" (
	"wxid" VARCHAR(64) PRIMARY KEY ,
	"name" VARCHAR(64) NULL,
	"phone" VARCHAR(20) NULL,
	"create_time" TIMESTAMP default (datetime('now', 'localtime'))
);
CREATE TABLE IF NOT EXISTS "MESSAGE" (
	"id" INTEGER PRIMARY KEY AUTOINCREMENT,
	"time" TIMESTAMP,
	"source" VARCHAR(20) NULL,
	"type" VARCHAR(20) NULL,
	"room_id" VARCHAR(30) NULL,
	"sender_id" VARCHAR(30) NULL,
	"sender_name" VARCHAR(255) NULL,
	"content" VARCHAR(2048) NULL
);
DELETE FROM sqlite_sequence;
COMMIT;
