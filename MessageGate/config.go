package main

import (
	"encoding/json"
	"io/ioutil"
	//"log"
)

type config struct {
	redis_ip         string
	redis_port       string
	redis_auth       string
	redis_recv_queue string
	redis_send_queue string
	nlp_ip           string
	nlp_port         string
}

func loadConfig() map[string]interface{} {
	data, err := ioutil.ReadFile("./config.json")
	if err != nil {
		return nil
	}

	var myConfig interface{}
	json.Unmarshal([]byte(string(data)), &myConfig)

	configResultTemp, ok := myConfig.(map[string]interface{})
	if ok {
		return configResultTemp
	}

	return nil
}
