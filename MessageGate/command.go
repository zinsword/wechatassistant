package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

func annoncement(msg *message, arg string) {
}

func sendRasaMsg(msg *message) {
	json.Marshal(msg)
}

func search(msg *message, arg string) {
	if strings.Contains(arg, "指标相似度") {
		finalArg := strings.Replace(arg, "指标相似度", "", -1)
		f, err := os.Open("./data/index")
		if err != nil {
			return
		}
		buf := bufio.NewReader(f)
		var sim = 0.0
		var simIndex = ""
		for {
			line, err := buf.ReadString('\n')
			line = strings.TrimSpace(line)

			var curSimilarity float64
			SimilarText(finalArg, line, &curSimilarity)
			//fmt.Println(finalArg, line, curSimilarity)
			if curSimilarity > sim {
				sim = curSimilarity
				simIndex = line
				msg.content = simIndex + " : " + strconv.FormatFloat(sim, 'f', -1, 64)
			}

			if err != nil {
				return
			}
		}
	}
}

func malfunction(msg *message, arg string) {
	str := "故障信息已收到,已安排后台人员排除，请留意相关通知"
	msg.content = str
}

func sendMsgToRasa(msg *message) {
	wxid := msg.senderID
	content := msg.content

	fmt.Println("sendMsgToRasa : ", wxid, content)
}

func usage(msg *message) {
	/*
		*-----*-----*----*
		|报障 | 通知 | 查询|
		*-----*-----*----*
	*/
	command := []rune(msg.content)
	switch string(command) {
	case "查询":
		str := "目前支持以下信息:\n"
		str += "|指标相似度|\n"
		str += "用法: 查询 指标相似度 XXX\n"
		msg.content = str
	case "通知":
		str := "用法: 通知 XXXXX\n"
		msg.content = str
	case "报障":
		str := "用法: 报障 XXXXX\n"
		msg.content = str
	default:
		str := "目前支持以下功能\n"
		str += "|查询|通知|报障|\n"
		str += "具体用法单独查询\n"
		str += "例如:查询怎么用?\n"
		str += "更多功能开发中[呲牙]\n"
		msg.content = str
	}
	msg.msgSource = "用法回复"
}

func innerHandleMsg(msg message, send *chan message) (string, bool) {
	content := msg.content
	if utf8.RuneCountInString(content) <= 2 {
		usage(&msg)
		//return "", true
	} else {

		command := string([]rune(content)[:2])
		arguments := string([]rune(content)[2:])
		switch command {
		case "查询":
			search(&msg, arguments)
			msg.msgSource = "查询回复"
		case "通知":
			annoncement(&msg, arguments)
			msg.msgSource = "通知回复"
		case "报障":
			malfunction(&msg, arguments)
			msg.msgSource = "报障回复"
		default:
			sendMsgToRasa(&msg)
			//msg.msgSource = "NLP机器人回复"
		}
	}
	msg.direction = "SEND"
	*send <- msg
	return "", true
}
