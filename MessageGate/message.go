package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/axgle/mahonia"
	"github.com/go-redis/redis"
)

type message struct {
	time       string
	direction  string
	msgSource  string
	msgType    string
	roomID     string
	senderID   string
	senderName string
	botID      string
	botName    string
	content    string
}

var db *sql.DB
var logger *log.Logger
var myConfig map[string]interface{}

var done = make(chan bool, 1)

const sendChannelSize = 255
const recvChannelSize = 255

var sendChannel = make(chan message, sendChannelSize)
var recvChannel = make(chan message, recvChannelSize)

func consumer(db *sql.DB) {
	logger.Println("Start listen to consumer queue ...")
	var ctx = context.Background()
	rdb := redis.NewClient(&redis.Options{
		Addr:     myConfig["redis_ip"].(string) + ":" + myConfig["redis_port"].(string),
		Password: myConfig["redis_auth"].(string),
		DB:       0,
	})
	logger.Println(myConfig["redis_recv_queue"].(string)+" at "+myConfig["redis_ip"].(string), myConfig["redis_port"].(string), myConfig["redis_auth"].(string))

	recvQueue, ok := myConfig["redis_recv_queue"].(string)
	if ok {
		pubsub := rdb.Subscribe(ctx, recvQueue)
		for {
			ch := pubsub.Channel()
			for msg := range ch {
				fmt.Println("RECV MSG : ", msg.Payload)
				logger.Println("RECV MSG : ", msg)
				msg.Payload = mahonia.NewDecoder("gbk").ConvertString(msg.Payload)
				msg.Payload = msg.Payload[1 : len(msg.Payload)-1]
				fmt.Println("After Decoded MSG : ", msg)
				var msgMap interface{}
				json.Unmarshal([]byte(msg.Payload), &msgMap)
				msgMapResult, ok := msgMap.(map[string]interface{})
				fmt.Println("Parser finish", msgMapResult, ok)
				if ok {
					newMsg := message{}
					newMsg.time = msgMapResult["timestamp"].(string)
					newMsg.msgSource = msgMapResult["source"].(string)
					newMsg.msgType = msgMapResult["type"].(string)
					newMsg.roomID = msgMapResult["wxid"].(string)
					newMsg.senderID = msgMapResult["msgSender"].(string)
					newMsg.senderName = msgMapResult["membernickname"].(string)
					newMsg.content = msgMapResult["content"].(string)
					newMsg.botID = msgMapResult["receiverID"].(string)
					newMsg.botName = msgMapResult["receiverName"].(string)
					newMsg.direction = "RECV"

					/*
						_, ret := insertMsg(db, newMsg)
						if !ret {
							logger.Println("Can not write to db --- ", newMsg)
						}
					*/
					go insertMsg(db, newMsg)
					recvChannel <- newMsg
				} else {
					logger.Println("Error while parse msg : ", msg)
				}
			}
		}
	} else {
		logger.Panic("Type error : ", recvQueue)
	}

	done <- true
}

func producer(db *sql.DB) {
	logger.Println("Start listen to producer queue ...")
	var ctx = context.Background()
	rdb := redis.NewClient(&redis.Options{
		Addr:     myConfig["redis_ip"].(string) + ":" + myConfig["redis_port"].(string),
		Password: myConfig["redis_auth"].(string),
		DB:       0, // use default DB
	})
	logger.Println(myConfig["redis_send_queue"].(string)+" at "+myConfig["redis_ip"].(string), myConfig["redis_port"].(string), myConfig["redis_auth"].(string))

	for {
		msg := <-sendChannel
		msg.time = time.Now().Format("2006/1/2 15:04:05")

		fmt.Println("SEND MSG : ", msg)
		logger.Println("SEND MSG : ", msg)
		/*
			newMsg := map[string]string{
				"timestamp":      mahonia.NewEncoder("GBK").ConvertString(msg.time),
				"direction":      mahonia.NewEncoder("GBK").ConvertString(msg.direction),
				"source":         mahonia.NewEncoder("GBK").ConvertString(msg.msgSource),
				"type":           mahonia.NewEncoder("GBK").ConvertString(msg.msgType),
				"wxid":           mahonia.NewEncoder("GBK").ConvertString(msg.roomID),
				"msgSender":      mahonia.NewEncoder("GBK").ConvertString(msg.senderID),
				"membernickname": mahonia.NewEncoder("GBK").ConvertString(msg.senderName),
				"content":        mahonia.NewEncoder("GBK").ConvertString(msg.content)}
		*/

		newMsg := map[string]string{
			"timestamp":      msg.time,
			"direction":      msg.direction,
			"source":         msg.msgSource,
			"type":           msg.msgType,
			"wxid":           msg.roomID,
			"msgSender":      msg.senderID,
			"membernickname": msg.senderName,
			"receiverID":     msg.botID,
			"receiverName":   msg.botName,
			"content":        msg.content}

		fmt.Println(newMsg)

		sendMsg, err := json.Marshal(newMsg)
		if err != nil {
			logger.Println("Paser send message error --- ", sendMsg)
			continue
		}

		finalMsg := mahonia.NewEncoder("GBK").ConvertString(string(sendMsg))

		err = rdb.Publish(ctx, myConfig["redis_send_queue"].(string), finalMsg).Err()
		if err != nil {
			logger.Println(err)
			continue
		}
		go insertMsg(db, msg)
	}
	time.AfterFunc(time.Second, func() {
		// When pubsub is closed channel is closed too.
		//_ = pubsub.Close()
	})
}

func handleMsg() {
	for {
		msg := <-recvChannel
		fmt.Println("Handle MSG : ", msg)
		go innerHandleMsg(msg, &sendChannel)
	}
}

func main() {
	file, err := os.OpenFile("./log/MessageGate.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
	if err != nil {
		fmt.Printf("Error :%s\n", err)
		os.Exit(1)
	}
	logger = log.New(file, "", log.LstdFlags|log.Llongfile)
	logger.Println("Init message gate service ...")

	myConfig = loadConfig()
	if myConfig == nil {
		logger.Println("Error while load config file ... exit !")
		os.Exit(1)
	}
	logger.Println("Load config : ", myConfig)

	db, ok := initDB()
	if !ok {
		logger.Println("Error while init db ... ", db)
		os.Exit(1)
	}
	defer closeDB(db)

	go consumer(db)
	go producer(db)
	go handleMsg()

	<-done
	logger.Println("All job done ... will exit!")
}
