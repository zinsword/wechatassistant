package main

import (
	"database/sql"

	//"fmt"

	_ "github.com/mattn/go-sqlite3"
)

//var db *sql.DB

var sqlCrateTable = `
CREATE TABLE IF NOT EXISTS "USER" (
	"wxid" VARCHAR(64) PRIMARY KEY ,
	"name" VARCHAR(64) NULL,
	"phone" VARCHAR(20) NULL,
	"create_time" TIMESTAMP default (datetime('now', 'localtime'))
);

CREATE TABLE IF NOT EXISTS "MESSAGE" (
	"id" INTEGER PRIMARY KEY AUTOINCREMENT,
	"direction" VARCHAR(20),
	"time" TIMESTAMP NULL,
	"source" VARCHAR(20) NULL,
	"type" VARCHAR(20) NULL,
	"room_id" VARCHAR(30) NULL,
	"sender_id" VARCHAR(30) NULL,
	"sender_name" VARCHAR(255) NULL,
	"bot_id" VARCHAR(255) NULL,
	"bot_name" VARCHAR(255) NULL,
	"content" VARCHAR(2048) NULL
);
`

func initDB() (*sql.DB, bool) {
	db, err := sql.Open("sqlite3", "./data/message.db")
	if err != nil {
		return nil, false
	}

	sqlTable := sqlCrateTable
	_, err = db.Exec(sqlTable)
	if err != nil {
		//fmt.Println(ret, err)
		return nil, false
	}
	db.Begin()
	//fmt.Println(db)
	return db, true
}

func insertMsg(db *sql.DB, msg message) (string, bool) {
	if db == nil {
		return "", false
	}

	stmt, err := db.Prepare("INSERT INTO MESSAGE(time, direction, source, type, room_id, sender_id, sender_name, bot_id, bot_name, content) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		return "", false
	}

	_, err = stmt.Exec(msg.time, msg.direction, msg.msgSource, msg.msgType, msg.roomID, msg.senderID, msg.senderName, msg.botID, msg.botName, msg.content)
	if err != nil {
		return "", false
	}
	return "", true
}

func updateUser(wxID, name, phone string) (string, bool) {
	return "", false
}

func closeDB(db *sql.DB) bool {
	db.Close()
	return true
}
